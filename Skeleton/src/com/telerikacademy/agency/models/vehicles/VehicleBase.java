package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.core.contracts.AgencyRepository;
import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.agency.commands.CommandsConstants.JOIN_DELIMITER;

public class VehicleBase implements Vehicle {

    private static final String TYPE_CANT_BE_NULL = "Type cannot be null";
    private static final double MIN_PRICE_PER_KILOMETER = 0.10;
    private static final double MAX_PRICE_PER_KILOMETER = 2.50;
    private static final int LAW_MIN_PASSENGER_CAPACITY = 1;
    private static final int LAW_MAX_PASSENGER_CAPACITY = 800;
    private int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;


    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = type;


    }

    private void setPassengerCapacity(int passengerCapacity) {
        if(passengerCapacity < LAW_MIN_PASSENGER_CAPACITY || passengerCapacity > LAW_MAX_PASSENGER_CAPACITY)
        {
            throw new IllegalArgumentException(String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",LAW_MIN_PASSENGER_CAPACITY,LAW_MAX_PASSENGER_CAPACITY));
        }
        this.passengerCapacity = passengerCapacity;
    }
    protected void validatePassengerInVehicle(int passengerCapacity,int min, int max)
    {
        if(passengerCapacity < min || passengerCapacity > max)
        {
            throw new IllegalArgumentException(String.format("A %s cannot have less than %d passengers or more than %d passengers.",getClass().getSimpleName(), min, max));
        }
    }


    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    private void setPricePerKilometer(double pricePerKilometer) {
        if(pricePerKilometer < MIN_PRICE_PER_KILOMETER || pricePerKilometer > MAX_PRICE_PER_KILOMETER)
        {
            throw  new IllegalArgumentException(String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",MIN_PRICE_PER_KILOMETER,MAX_PRICE_PER_KILOMETER));
        }
        this.pricePerKilometer = pricePerKilometer;
    }
    @Override
    public String print()
    {
        return  String.format(
            "Passenger capacity: %d\n" +
            "Price per kilometer: %.2f\n" +
            "Vehicle type: %s\n",getPassengerCapacity(),getPricePerKilometer(),this.type.toString());
    }
    @Override
    public String toString() {
        return print();
    }
}
    //