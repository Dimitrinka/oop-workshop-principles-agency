package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private static final int MIN_PASSENGER_IN_BUS = 10;
    private static final int MAX_PASSENGER_IN_BUS = 50;

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity,pricePerKilometer,VehicleType.LAND);
        super.validatePassengerInVehicle(passengerCapacity,MIN_PASSENGER_IN_BUS,MAX_PASSENGER_IN_BUS);

    }
    @Override
    public String print() {
        return String.format("%s ---\n%s","Bus",super.print());
    }
    @Override
    public String toString() {
        return print();
    }




}
