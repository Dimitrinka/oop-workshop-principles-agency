package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

import java.util.List;

public class TrainImpl extends VehicleBase implements Train{
    public static final int CART_MIN = 1;
    public static final int CART_MAX = 15;
    private int cart;
    private static final int MIN_PASSENGER_IN_TRAIN = 50;
    private static final int MAX_PASSENGER_IN_TRAIN = 150;


    public TrainImpl(int passengerCapacity, double pricePerKilometer, int cart) {
        super(passengerCapacity,pricePerKilometer,VehicleType.LAND);
        super.validatePassengerInVehicle(passengerCapacity,MIN_PASSENGER_IN_TRAIN,MAX_PASSENGER_IN_TRAIN);
        setCart(cart);
    }

    @Override
    public int getCarts() {
        return cart;
    }

    private void setCart(int cart) {
        if(cart < CART_MIN || cart > CART_MAX)
        {
            throw new IllegalArgumentException(String.format("A train cannot have less than %d cart or more than %d carts.",CART_MIN,CART_MAX));
        }
        this.cart = cart;
    }

    /*@Override
    public int getPassengerCapacity() {
        return 0;
    }

    @Override
    public double getPricePerKilometer() {
        return 0;
    }

    @Override
    public VehicleType getType() {
        return null;
    }*/

    @Override
    public String print() {
        return String.format("%s ---\n%sCarts amount: %d\n","Train",super.print(),getCarts());
    }
    @Override
    public String toString() {
        return print();
    }
}
