package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Airplane;

public class AirplaneImpl extends VehicleBase implements Airplane {
   /* private static final int MIN_PASSENGER_IN_AIRPLANE = 50;
    private static final int MAX_PASSENGER_IN_AIRPLANE= 150;*/
    private boolean hasFreeFood;
    public AirplaneImpl(int passengerCapacity, double pricePerKilometer, boolean hasFreeFood) {
        super(passengerCapacity,pricePerKilometer,VehicleType.AIR);
       /* super.validatePassengerInVehicle(passengerCapacity,MIN_PASSENGER_IN_AIRPLANE,MAX_PASSENGER_IN_AIRPLANE);*/
        this.hasFreeFood = hasFreeFood;
    }

    @Override
    public boolean hasFreeFood() {
        return hasFreeFood;
    }

    @Override
    public String print() {
        return String.format("%s ---\n%sHas free food: %b\n","Airplane",super.print(),hasFreeFood);
    }
    @Override
    public String toString() {
        return print();
    }
}
